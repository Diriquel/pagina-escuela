function myMap() {
    var mapOptions = {
        center: new google.maps.LatLng(-36.739816, -73.093593),
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.TERRAIN
    }
var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}